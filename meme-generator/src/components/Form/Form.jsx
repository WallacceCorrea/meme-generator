import { useState, useEffect } from "react";

import "./Form.css";
import Meme from "../Meme/Meme";

let url;

export default function Form() {
    const [meme, setMeme] = useState({
    topText: "",
    bottomText: "",
    randomImage: "http://i.imgflip.com/1bij.jpg",
  });
/**
    useEffect takes a function as its parameter. If that function
    returns something, it needs to be a cleanup function. Otherwise,
    it should return nothing. If we make it an async function, it
    automatically retuns a promise instead of a function or nothing.
    Therefore, if you want to use async operations inside of useEffect,
    you need to define the function separately inside of the callback
    function, as seen below:
    */
   
  const memesData = useEffect(() => {
    async function getMemes() {
      const res = await fetch("https://api.imgflip.com/get_memes")
      const data = await res.json()
      setAllMemeImages(data.data.memes)
    }
    getMemes()
  }, [])


  const [allMemeImages, setAllMemeImages] = useState(memesData)

  
  function handleChange(event) {
    const {name, value} = event.target
    setMeme(prevMeme => ({
      ...prevMeme,
      [name]: value
    }))
  }

  function getImage() {
    const allMemes = allMemeImages;
    const randomNumber = Math.floor(Math.random() * allMemes.length);
    const url = allMemes[randomNumber].url;
    return url;
  }

  function getNewMeme() {
    setMeme(prevMeme => ({
        ...prevMeme,
        randomImage: getImage()
    }));
  }

  return (
    <div className="container">
      <div className="form">
        <input
          type="text"
          name="topText"
          id="top-text"
          placeholder="Top line"
          onChange={handleChange}
        />
        <input
          type="text"
          name="bottomText"
          id="bottom-line"
          placeholder="Bottom line"
          onChange={handleChange}
        />
        <button type="submit" onClick={getNewMeme}>
          Get a new meme image 🖼
        </button>
      </div>
      <Meme
        img={meme.randomImage}
        topText={meme.topText}
        bottomText={meme.bottomText}
      ></Meme>
    </div>
  );

 
}
