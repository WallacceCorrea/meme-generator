import "./Header.css";

const  logoImg ='./../src/assets/troll-face.png'



export default function Header() {
  return (
    <div className="nav-container">
      <div className="nav-logo">
        <img className="logo-img" src={logoImg} alt="Brand visual identity" />
        <span className="logo-text">Meme Generator</span>
      </div>
      <div className="nav-title">React Course - Project 3</div>
    </div>
  );
}
