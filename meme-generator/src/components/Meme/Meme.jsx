import "./Meme.css";

export default function Meme(props) {
  return (
    <div className="meme-img">
      <img src={props.img} alt="Random meme" />
      <h2 className="meme-text top">{props.topText}</h2>
      <h2 className="meme-text bottom">{props.bottomText}</h2>
    </div>
  );
}
